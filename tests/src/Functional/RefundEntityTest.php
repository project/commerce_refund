<?php

namespace Drupal\Tests\commerce_refund\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_refund\Entity\Refund;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;
use Drupal\user\UserInterface;

/**
 * Test the refund entity.
 *
 * @coversDefaultClass \Drupal\commerce_refund\Entity\Refund
 *
 * @group commerce_refund
 */
class RefundEntityTest extends OrderKernelTestBase {

  /**
   * Default theme name for this test.
   *
   * @var string
   */
  public string $defaultTheme = 'stack';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  public static $modules = [
    'commerce_payment',
    'commerce_payment_example',
    'commerce_refund',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected OrderInterface $order;

  /**
   * {@inheritdoc}
   *
   */
  protected function setUp(): void {
    parent::setUp();
    // The follow lines is copy from
    // Drupal\Tests\commerce_payment\Kernel\Entity\PaymentTest.
    $this->installEntitySchema('commerce_payment');
    $this->installConfig('commerce_payment');

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    if ($user instanceof UserInterface) {
      $this->user = $user;
    }
  }

  /**
   * Tests that the refund content entity can be created.
   *
   * @covers ::id
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRefundEntity() {
    // Create a product with a product variation.
    // Create an order.
    // Make the order paid.
    // Refund the payment.
    $variation1 = ProductVariation::create([
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 0,
    ]);
    $variation1->save();
    $variation2 = ProductVariation::create([
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
    ]);
    $variation2->save();
    $product = Product::create([
      'type' => 'default',
    ]);
    $product->save();
    $order_item = OrderItem::create([
      'title' => 'Membership subscription',
      'type' => 'test',
      'quantity' => 1,
      'unit_price' => [
        'number' => '30.00',
        'currency_code' => 'USD',
      ],
    ]);
    $order_item->save();
    $order = Order::create([
      'type' => 'default',
      'uid' => $this->user->id(),
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
    ]);
    $order->save();
    $order = $this->reloadEntity($order);
    if ($order instanceof OrderInterface) {
      $this->order = $order;
    }
    $payment_gateway = PaymentGateway::create([
      'id' => 'example',
      'label' => 'Example',
      'plugin' => 'example_onsite',
    ]);
    $payment_gateway->save();
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $this->order->getBalance(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $this->order->id(),
    ]);
    $refund = Refund::create([
      'payment_id' => $payment->id(),
      'refund_number' => '123456789',
      'remote_id' => '123456',
      'remote_state' => 'success',
      'amount' => new Price('123', 'USD'),
      'remark' => 'OK!',
      'state' => 'new',
    ]);
    $this->assertEquals($payment->id(), $refund->getPaymentId());
    $this->assertEquals('123456789', $refund->getRefundNumber());
    $this->assertEquals('123456', $refund->getRemoteId());
  }

}
