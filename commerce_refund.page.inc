<?php

/**
 * @file
 * Contains commerce_refund.page.inc.
 *
 * Page callback for Refund entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Refund templates.
 *
 * Default template: commerce_refund.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 *
 * @noinspection PhpUnused
 */
function template_preprocess_commerce_refund(array &$variables) {
  // Fetch Refund Entity Object.
  $variables['commerce_refund'] = $variables['elements']['#commerce_refund'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
