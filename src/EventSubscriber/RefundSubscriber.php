<?php

namespace Drupal\commerce_refund\EventSubscriber;

use Drupal\commerce_payment\Event\PaymentRefundEvent;
use Drupal\commerce_payment\Event\PaymentRefundEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Refund subscriber to track transaction state.
 */
class RefundSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[PaymentRefundEvents::PAYMENT_REFUND_CREATED][] = ['onCreate'];
    $events[PaymentRefundEvents::PAYMENT_REFUND_SUCCESS][] = ['onSuccess'];
    $events[PaymentRefundEvents::PAYMENT_REFUND_FAILED][] = ['onFailed'];
    return $events;
  }

  /**
   * Create a refund entity.
   *
   * @param \Drupal\commerce_payment\Event\PaymentRefundEvent $event
   *   The event to process.
   */
  public function onCreate(PaymentRefundEvent $event) {
    // Load refund entity by payment id and tracking id.
    // create one if refund entity can not be found.
  }

  /**
   * Apply the refund.
   *
   * @param \Drupal\commerce_payment\Event\PaymentRefundEvent $event
   *   The event to process.
   */
  public function onSuccess(PaymentRefundEvent $event) {}

  /**
   * Rollback the refund.
   *
   * @param \Drupal\commerce_payment\Event\PaymentRefundEvent $event
   *   The event to process.
   */
  public function onFailed(PaymentRefundEvent $event) {}

}
