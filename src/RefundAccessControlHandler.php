<?php

namespace Drupal\commerce_refund;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Refund entity.
 *
 * @see \Drupal\commerce_refund\Entity\Refund.
 *
 * @noinspection PhpUnused
 */
class RefundAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\commerce_refund\Entity\RefundInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view refund entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit refund entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete refund entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add refund entities');
  }

}
