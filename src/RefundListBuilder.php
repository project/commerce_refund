<?php

namespace Drupal\commerce_refund;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Refund entities.
 *
 * @ingroup commerce_refund
 *
 * @noinspection PhpUnused
 */
class RefundListBuilder extends EntityListBuilder {

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected CurrencyFormatterInterface $currencyFormatter;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $currentRouteMatch;

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('commerce_price.currency_formatter'),
      $container->get('current_route_match')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    CurrencyFormatterInterface $currency_formatter,
    RouteMatchInterface $current_route_match
  ) {
    parent::__construct($entity_type, $storage);
    $this->currencyFormatter = $currency_formatter;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * Get the commerce_price.currency_formatter service.
   */
  private function currencyFormatter(): CurrencyFormatterInterface {
    return $this->currencyFormatter;
  }

  /**
   * Get the current_route_match service.
   */
  private function getRouteMatch(): RouteMatchInterface {
    return $this->currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public function load(): array {
    $order = $this->getRouteMatch()->getParameter('commerce_order');
    return $this->loadMultipleByOrder($order);
  }

  /**
   * Load all of a given order.
   */
  public function loadMultipleByOrder($order): array {
    $query = $this->storage->getQuery()
      ->condition('payment_id.entity.order_id', $order)
      ->sort('id');
    $result = $query->execute();

    return $result ? $this->storage->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('Refund ID');
    $header['remark'] = $this->t('Remarks');
    $header['amount'] = $this->t('Amount');
    $header['state'] = $this->t('State');
    $header['remote_state'] = $this->t('Remote state');
    $header['remote_id'] = $this->t('Remote ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\commerce_refund\Entity\Refund $entity */
    $row['id'] = $entity->id();
    $row['remark'] = $entity->label();

    $amount = $entity->getAmount();
    $formatted_amount = $this->currencyFormatter()->format($amount->getNumber(), $amount->getCurrencyCode());

    $row['amount'] = $formatted_amount;
    $row['state'] = $entity->getState()->getLabel();
    $row['remote_state'] = $entity->getRemoteState();
    $row['remote_id'] = $entity->getRemoteId() ?: $this->t('N/A');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getDefaultOperations(EntityInterface $entity): array {
    $operations = [];
    if ($entity->access('update') && $entity->hasLinkTemplate('refresh-form')) {
      $operations['refresh'] = [
        'title' => $this->t('Refresh'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('refresh-form')),
      ];
    }
    return $operations;
  }

}
