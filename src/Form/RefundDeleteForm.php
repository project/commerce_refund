<?php

namespace Drupal\commerce_refund\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Refund entities.
 *
 * @ingroup commerce_refund
 *
 * @noinspection PhpUnused
 */
class RefundDeleteForm extends ContentEntityDeleteForm {}
