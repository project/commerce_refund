<?php

namespace Drupal\commerce_refund\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Refund edit forms.
 *
 * @ingroup commerce_refund
 *
 * @noinspection PhpUnused
 */
class RefundForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage(
            $this->t(
              'Created the %label Refund.',
              [
                '%label' => $entity->label(),
              ]
            )
          );
        break;

      default:
        $this->messenger()
          ->addMessage(
            $this->t(
              'Saved the %label Refund.',
              [
                '%label' => $entity->label(),
              ]
            )
          );
    }
    $form_state->setRedirect(
      'entity.commerce_refund.canonical',
      [
        'commerce_refund' => $entity->id(),
      ]
    );

    return $status;
  }

}
