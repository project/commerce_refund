<?php

namespace Drupal\commerce_refund\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for cancel and refund order operation.
 *
 * @noinspection PhpUnused
 */
class OrderCancelAndRefundForm extends ConfirmFormBase {

  /**
   * The order to operate.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  private $order;

  /**
   * The "entity_type.manager" service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Init the form object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->order = Order::load(
      $this->getRouteMatch()
        ->getParameter('commerce_order')
    );
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_refund_order_cancel_and_refund_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t(
      'Are you sure to operate Cancel And Refund on order: %label ?',
      [
        '%label' => $this->order->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->order;

    // Indicate the order support cancel or not.
    $transitions = $order->getState()->getTransitions();
    if (isset($transitions['cancel'])) {
      // Refund all the payments.
      if ($this->refundOrder($order)) {
        $order->getState()->applyTransition($transitions['cancel']);
        $order->save();
        $this->messenger()
          ->addMessage(
            $this->t(
              'Order %label have been cancel and refund all payments.',
              [
                '%label' => $order->label(),
              ]
            )
          );
      }
    }
    else {
      $this->messenger()
        ->addError(
          $this->t(
            'Order %label does not support this operation currently.',
            [
              '%label' => $order->label(),
            ]
          )
        );
    }

    $form_state->setRedirectUrl($order->toUrl('collection'));
  }

  /**
   * Refund all payments belong to an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to refund.
   *
   * @return bool
   *   Indicate all refund successfully or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function refundOrder(OrderInterface $order): bool {
    // Refund all the payments belong to this order.
    /** @var \Drupal\commerce_payment\PaymentStorage $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $payment_storage->loadMultipleByOrder($order);
    $all_refund_success = TRUE;
    foreach ($payments as $payment) {
      if ($payment->getRefundedAmount()->isZero()) {
        try {
          $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
          if ($payment_gateway_plugin instanceof SupportsRefundsInterface) {
            $payment_gateway_plugin->refundPayment($payment, $payment->getAmount());
            $this->messenger()
              ->addMessage(
                $this->t(
                  'Payment %label have been refund successfully.',
                  [
                    '%label' => $payment->id(),
                  ]
                )
              );
          }
          else {
            $this->messenger()
              ->addWarning(
                $this->t(
                  'Gateway of payment %label does not support refund operation, skipped it.',
                  [
                    '%label' => $payment->id(),
                  ]
                )
              );
          }
        }
        catch (\Exception $exception) {
          $this->messenger()
            ->addError(
              $this->t(
                'Payment %label refund fail : %msg',
                [
                  '%label' => $payment->id(),
                  '%msg' => $exception->getMessage(),
                ]
              )
            );
          $all_refund_success = FALSE;
        }
      }
      else {
        $this->messenger()
          ->addWarning(
            $this->t(
              'Payment %label had been refund before, skipped it.',
              [
                '%label' => $payment->id(),
              ]
            )
          );
      }
    }

    return $all_refund_success;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getCancelUrl(): Url {
    return $this->order->toUrl('collection');
  }

}
