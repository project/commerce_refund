<?php

namespace Drupal\commerce_refund\Form;

use Drupal\commerce_refund\SupportsRefreshRefundInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Form for cancel and refund order operation.
 *
 * @ingroup commerce_refund
 *
 * @noinspection PhpUnused
 */
class RefundRefreshForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure to refresh refund state from remote gateway?');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_refund\Entity\RefundInterface $refund */
    $refund = $this->getEntity();

    $gateway_plugin = $refund->getPayment()->getPaymentGateway()->getPlugin();
    if ($gateway_plugin instanceof SupportsRefreshRefundInterface) {
      $gateway_plugin->refreshRefund($refund);
    }

    $form_state->setRedirectUrl($refund->toUrl('collection'));
    $this->messenger()->addStatus($this->t('Refreshed!'));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getCancelUrl(): Url {
    $entity = $this->getEntity();
    if ($entity->hasLinkTemplate('collection')) {
      // If available, return the collection URL.
      return $entity->toUrl('collection');
    }
    else {
      // Otherwise fall back to the default link template.
      return $entity->toUrl();
    }
  }

}
