<?php

namespace Drupal\commerce_refund\Entity;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;

/**
 * Provides an interface for defining Refund entities.
 *
 * @ingroup commerce_refund
 */
interface RefundInterface extends
    ContentEntityInterface,
    EntityChangedInterface {

  /**
   * Gets the parent order.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The order entity, or null.
   */
  public function getPayment(): ?PaymentInterface;

  /**
   * Gets the parent order ID.
   *
   * @return int|null
   *   The order ID, or null.
   */
  public function getPaymentId(): ?int;

  /**
   * Gets the refund number.
   *
   * @return string|null
   *   The refund number.
   */
  public function getRefundNumber(): ?string;

  /**
   * Sets the refund number.
   *
   * @param string $refund_number
   *   The refund number.
   *
   * @return static
   */
  public function setRefundNumber(string $refund_number): self;

  /**
   * Gets the payment remote ID.
   *
   * @return string|null
   *   The payment remote ID.
   */
  public function getRemoteId(): ?string;

  /**
   * Sets the payment remote ID.
   *
   * @param string $remote_id
   *   The payment remote ID.
   *
   * @return static
   */
  public function setRemoteId(string $remote_id): self;

  /**
   * Gets the payment remote state.
   *
   * @return string|null
   *   The payment remote state.
   */
  public function getRemoteState(): ?string;

  /**
   * Sets the payment remote state.
   *
   * @param string $remote_state
   *   The payment remote state.
   *
   * @return static
   */
  public function setRemoteState(string $remote_state): self;

  /**
   * Gets the payment amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL.
   */
  public function getAmount(): ?Price;

  /**
   * Sets the payment amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The payment amount.
   *
   * @return static
   */
  public function setAmount(Price $amount): self;

  /**
   * Gets the payment state.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The payment state.
   */
  public function getState(): StateItemInterface;

  /**
   * Sets the payment state.
   *
   * @param string $state_id
   *   The new state ID.
   *
   * @return static
   */
  public function setState(string $state_id): self;

  /**
   * Gets the Distributor remark.
   *
   * @return string|null
   *   Remark of the Distributor.
   */
  public function getRemark(): ?string;

  /**
   * Sets the Distributor remark.
   *
   * @param string $remark
   *   The Distributor remark.
   *
   * @return static
   */
  public function setRemark(string $remark): self;

  /**
   * Gets whether the payment has been completed.
   *
   * @return bool
   *   TRUE if the payment has been completed, FALSE otherwise.
   */
  public function isCompleted(): bool;

  /**
   * Gets the payment completed timestamp.
   *
   * @return int
   *   The payment completed timestamp.
   */
  public function getCompletedTime(): int;

  /**
   * Sets the payment completed timestamp.
   *
   * @param int $timestamp
   *   The payment completed timestamp.
   *
   * @return static
   */
  public function setCompletedTime(int $timestamp): self;

  /**
   * Gets the Refund creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Refund.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Refund creation timestamp.
   *
   * @param int $timestamp
   *   The Refund creation timestamp.
   *
   * @return static
   *   The called Refund entity.
   */
  public function setCreatedTime(int $timestamp): self;

}
