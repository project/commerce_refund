<?php

namespace Drupal\commerce_refund\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Refund entities.
 *
 * @noinspection PhpUnused
 */
class RefundViewsData extends EntityViewsData {}
