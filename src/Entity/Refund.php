<?php

namespace Drupal\commerce_refund\Entity;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\entity\BundleFieldDefinition;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;

/**
 * Defines the Refund entity.
 *
 * @ingroup commerce_refund
 *
 * @ContentEntityType(
 *   id = "commerce_refund",
 *   label = @Translation("Refund"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_refund\RefundListBuilder",
 *     "views_data" = "Drupal\commerce_refund\Entity\RefundViewsData",
 *     "form" = {
 *       "default" = "Drupal\commerce_refund\Form\RefundForm",
 *       "refresh" = "Drupal\commerce_refund\Form\RefundRefreshForm",
 *       "delete" = "Drupal\commerce_refund\Form\RefundDeleteForm",
 *     },
 *     "access" = "Drupal\commerce_refund\RefundAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_refund\RefundHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "commerce_refund",
 *   admin_permission = "administer refund entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "remark",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/orders/{commerce_order}/refunds/{commerce_refund}",
 *     "refresh-form" = "/admin/commerce/orders/{commerce_order}/refunds/{commerce_refund}/refresh",
 *     "delete-form" = "/admin/commerce/orders/{commerce_order}/refunds/{commerce_refund}/delete",
 *     "collection" = "/admin/commerce/orders/{commerce_order}/refunds",
 *   },
 *   field_ui_base_route = "commerce_refund.settings"
 * )
 */
class Refund extends ContentEntityBase implements RefundInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['commerce_order'] = $this->getPayment()->getOrderId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getPayment(): ?PaymentInterface {
    // @phpstan-ignore-next-line
    return $this->get('payment_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentId(): ?int {
    // @phpstan-ignore-next-line
    return $this->get('payment_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefundNumber(): string {
    return $this->get('refund_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefundNumber($refund_number): self {
    $this->set('refund_number', $refund_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId(): string {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id): self {
    $this->set('remote_id', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteState(): string {
    return $this->get('remote_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteState($remote_state): self {
    $this->set('remote_state', $remote_state);
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getState(): StateItemInterface {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state_item */
    $state_item = $this->get('state')->first();
    return $state_item;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state_id): self {
    $this->set('state', $state_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getAmount(): ?Price {
    if (!$this->get('amount')->isEmpty()) {
      /** @var \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem $price */
      $price = $this->get('amount')->first();
      return $price->toPrice();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount(Price $amount): self {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemark(): ?string {
    return $this->get('remark')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemark(string $remark): self {
    $this->set('remark', $remark);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isCompleted(): bool {
    return !$this->get('completed')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedTime(): int {
    return $this->get('completed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCompletedTime(int $timestamp): self {
    $this->set('completed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): self {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['payment_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment'))
      ->setDescription(t('The parent payment.'))
      ->setSetting('target_type', 'commerce_payment')
      ->setReadOnly(TRUE);

    $fields['refund_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Unique refund number'))
      ->setDescription(t('Unique refund number in local system.'))
      ->setRequired(TRUE);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setDescription(t('The remote refund ID to track the refund transaction state.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote State'))
      ->setDescription(t('The remote refund state.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Amount'))
      ->setDescription(t('The refund amount.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('State machine for the refund.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow', 'refund_default');

    $fields['remark'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remarks'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ]);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Completed'))
      ->setDescription(t('The time when the refund was completed.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
