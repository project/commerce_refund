<?php

namespace Drupal\commerce_refund;

use Drupal\commerce_refund\Entity\RefundInterface;

/**
 * Defines the interface for gateways which support refresh refund.
 */
interface SupportsRefreshRefundInterface {

  /**
   * Refresh state of a given refund entity from remote api.
   */
  public function refreshRefund(RefundInterface $refund);

}
