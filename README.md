# Commerce Refund

Provides enhanced refund functionalities to Drupal Commerce payment system.

## Features

* Provides a content entity type `commerce_refund` to track the detail lifecycle of each payment refund process.
* Provides an operation link `Cancel and refund`for order entity,
  only who has the `cancel commerce order and refund payment` permission can see this link.
* Provide a `refresh` operation on `commerce_refund` entity, which can fetch the remote transaction state
  immediately manually. The prerequisite is the payment gateway support the `SupportsRefreshRefundInterface` interface.
* Provide `RefundSubscriber` to integrate with Commerce core's refund operation,
  but this need the [issue#3121116](https://www.drupal.org/project/commerce/issues/3121116) be solved.
